module.exports = {
  root: true,
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: ['standard', 'plugin:vue/recommended', 'plugin:prettier/recommended'],
  // required to lint *.vue files
  plugins: ['prettier', 'html'],
  settings: {
    'html/html-extensions': ['.html']
  },
  // add your custom rules here
  rules: {
    'prettier/prettier': ['error'],
    quotes: ['error', 'single'],
    'vue/component-name-in-template-casing': ['error', 'PascalCase'],
    // allow debugger during development
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // Turned off spaces before pern because of broken Beautify formating for JS inside script tag
    'space-before-function-paren': 'off',
    'vue/max-attributes-per-line': 'off',
    allowFirstLine: 'off',
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'always',
          normal: 'always',
          component: 'always'
        },
        svg: 'always',
        math: 'always'
      }
    ]
  }
}
