import Vue from 'vue'
import router from './routes'
import store from 'Store/index.js'
import App from './App.vue'

window.app = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
