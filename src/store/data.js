import api from 'Api/google.js'
import uniqid from 'uniqid'

const state = {
  loadingStatus: 'notLoading',
  sources: [],
  rows: [],
  headers: []
}

const mutations = {
  SET_LOADING_STATUS(state, status) {
    state.loadingStatus = status
  },
  ADD_SOURCE(state, item) {
    item.id = uniqid()
    state.sources.push(item)
  },
  REMOVE_SOURCE(state, id) {
    state.sources = state.sources.filter(src => src.link !== id)
  },
  SET_DATA(state, data) {
    state.rows = data
    state.headers = data.columns
  }
}
const actions = {
  fetchSource({ rootState, state, commit }, id) {
    commit('SET_LOADING_STATUS', 'loading')
    const src = state.sources.find(src => src.id === id)
    var url
    switch (src.type) {
      case 'google':
        // https://www.googleapis.com/drive/v3/files/1FgZYFYUGCdefeeDcmsvf3iY7ZAn56RXJDWvQEFVWZ7Y/export?mimeType=text/csv"
        url = `https://www.googleapis.com/drive/v3/files/${src.link}/export?mimeType=text/csv`
        break
      default:
        // default 'url' should be a valid url anyway.
        break
    }
    api
      .getData(url, rootState.auth.token)
      .then(response => {
        commit('SET_LOADING_STATUS', 'success')
        commit('SET_DATA', response)
      })
      .catch(fail => {
        commit('SET_LOADING_STATUS', 'failed')
      })
  },
  addSource({ commit }, item) {
    commit('ADD_SOURCE', item)
  },
  removeSource({ commit }, id) {
    commit('REMOVE_SOURCE', id)
  }
}
const getters = {
  filterSource(state) {
    //TODO: Replace view component filtered()
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
