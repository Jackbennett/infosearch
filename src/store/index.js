import Vue from 'vue'
import Vuex from 'vuex'
import vuexPersistedState from 'vuex-persistedstate'

import auth from 'Store/auth.js'
import data from 'Store/data.js'

Vue.use(Vuex)
Vue.config.devtools = true

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  strict: debug,
  modules: {
    auth,
    data
  },
  plugins: [
    vuexPersistedState({
      storage: window.localStorage,
      paths: ['data.sources', 'auth.name', 'auth.token']
    })
  ]
})
