import api from 'Api/google.js'

const state = {
  name: '',
  token: '',
  expire: '',
  // status = wait -> success/fail -> error
  status: '',
  developerKey: api.developerKey
}

const REQUEST = 'REQUEST'
const SUCCESS = 'SUCCESS'
const ERROR = 'ERROR'
const SIGNOUT = 'SIGNOUT'

const mutations = {
  [REQUEST](state) {
    state.status = 'waiting'
  },
  [SUCCESS](state, identity) {
    state.status = 'success'
    state.name = identity.name
    state.token = identity.token
    state.expire = identity.expire
  },
  [ERROR](state) {
    state.status = 'fail'
  },
  [SIGNOUT](state) {
    state.status = 'none'
    state.name = null
    state.token = null
    state.expire = null
  }
}

const actions = {
  [REQUEST]({ commit }) {
    commit(REQUEST)
    api
      .signIn()
      .then(resp => {
        commit(SUCCESS, resp)
      })
      .catch(err => {
        commit(ERROR)
      })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
