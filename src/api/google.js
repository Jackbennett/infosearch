import Vue from 'vue'
import gAuth from 'vue-google-oauth2'
import { csv } from 'd3-fetch'

const oauthClientID = process.env.GOOGLE_OAUTH_CLIENTID
const developerKey = process.env.DEVELOPER_KEY
if (oauthClientID) {
  Vue.use(gAuth, {
    clientId: oauthClientID,
    scope: 'profile email https://www.googleapis.com/auth/drive.file'
  })
}

export default {
  oauthClientID,
  developerKey,
  signIn() {
    return new Promise((resolve, reject) => {
      Vue.prototype.$gAuth
        .signIn()
        .then(resp => {
          resolve({
            name: resp.getBasicProfile().Bd,
            token: resp.wc.access_token,
            expire: resp.wc.expires_in
          })
        })
        .catch(err => reject)
    })
  },
  getData(path, token) {
    return csv(path, {
      methods: 'get',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }
}
