import Vue from 'vue'
import Router from 'vue-router'

import Home from './Home.vue'
import Info from './Info.vue'
import fourOhFour from './404.vue'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: __dirname,
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/info/:id', name: 'info', component: Info, props: true },
    { path: '*', name: '404', component: fourOhFour }
  ]
})
