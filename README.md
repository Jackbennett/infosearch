# Fuzzy Search for information

- Create a list of information with a fuzzy-search of the data within them.
- Share a Google Sheet with the dashboard and use your organizations existing ACL workflow

## Syntax header:contents or just contents

Text on its own is compared with the text in all cells. Prefix it will text that matches some of the column name and a colon to limit that search to that column. Use a space to separate multiple filters.

![demo video](./docs/media/info-demo.webm)
_Please note:_ Any names or details have been generated randomly and and likeness is coincidental.

If you know what you need put it on the left side of the colon, filter with what you know e.g. extension:2

# Features / plans

- Use any Google Sheet your login has access to
- Filter values based on text
- Filter value specific to keys matched preceding `:`
- New url once data source is selected. App url hash is mapped to the sheets import url, it's not just the sheet ID

### 0.5 Multi Tenancy / dashboard

- enable app to have a dashboard passed in for the base route.

### 0.9

- Replace localstorage of auth
- Change storage for session or cookies. re-evaluate caching the data (might leave all data access to googles auth for users to control)

### 1.1

- highlight matches in results
- rename column heading from source in admin area
- Set hide/show columns in admin area

### 1.2

- Authorize app for gsutie domain from marketplace.

## Future

- MS OneDrive/sharepoint integration
- Simple CSV Upload (con's: data protection implications)
